defmodule Cineque do
  @moduledoc """
  Documentation for Cineque.
  """

  @doc """
  Hello world.

  ## Examples
      iex> job = %Cineque.Job{resource: "some/resource:1", object: "some/key/object.mov", events_callback_url: "https://example.com/media/2/events"}
      iex> Cineque.enqueue(job)
      :ok

  """
  alias Cineque.Job

  use Tesla

  @spec enqueue(Cineque.Job.t()) :: {:error, :not_enqueued} | {:ok, :enqueued}
  def enqueue(%Job{source: %{endpoint: source_endpoint}} = job) do
    client = client(source_endpoint)

    case post(client, "/jobs", job) do
      {:ok, %Tesla.Env{status: 201}} -> {:ok, :enqueued}
      _ -> {:error, :not_enqueued}
    end
  end

  defp client(source_endpoint) do
    endpoint = Application.get_env(:cineque, :cineplex) || "https://cineplex.api.upmaru.studio"

    middleware = [
      {Tesla.Middleware.BaseUrl, endpoint},
      Tesla.Middleware.JSON,
      {Tesla.Middleware.Headers, [{"x-source", source_endpoint}]}
    ]

    Tesla.client(middleware)
  end
end
