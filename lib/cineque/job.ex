defmodule Cineque.Job do
  @derive {Jason.Encoder, only: [:object, :resource, :events_callback_url]}

  defstruct [:object, :resource, :events_callback_url, :source]

  @type t() :: %__MODULE__{
    object: String.t(),
    resource: String.t(),
    events_callback_url: String.t(),
    source: %{
      endpoint: binary()
    }
  }
end
